# Summary

* [Introduction](README.md)
* [參數設定](can-shu-she-ding.md)
* [Tex 大大](t1.md)
  * [1-1](t1/1-1.md)
  * [1-2](t1/1-2.md)
  * [1-3](t1/1-3.md)
  * [1-4](t1/1-4.md)
* [Jack私人筆記](t2.md)
* [2-1](t2/2-1.md)
* [BUG底加](t1/1-5.md)
* [PPT專區](t3.md)
  * [3-1](t3/3-1.md)
  * [3-2](t3/3-2.md)

