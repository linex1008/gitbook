#參數設定

---

###Vocabulary Size
基於詞彙頻率保留的最大字數。

---

###Sequence Length

---

###Epoch
可為任意數值，通常定義為"一次遍歷整個訓練資料集"，用於將訓練分成不同階段，這對於紀錄和定期評估很有用。

---

###Batch Size
一組N個樣本。批量處理中的樣本是並行處理的。在訓練模型時，一個Batch訓練結果只會更新一次model。

---

###Learning Rate
學習率會影響 loss 收斂速度

---

###Dropout Rate
```python
keras.callbacks.EarlyStopping(monitor='val_loss',patience=0)
```
當被監測的validation loss不再下降，則停止訓練。
####參數
* **patience**：沒有進步的訓練輪數，在這之後訓練會被停止。

---

###Early Stop Patience
```python
keras.callbacks.EarlyStopping(monitor='val_loss',patience=0)
```
當被監測的validation loss不再下降，則停止訓練。
####參數
* **patience**：沒有進步的訓練輪數，在這之後訓練會被停止。

---

###Early Stop Patience
```python
keras.callbacks.EarlyStopping(monitor='val_loss',patience=0)
```
當被監測的validation loss不再下降，則停止訓練。
####參數
* **patience**：沒有進步的訓練輪數，在這之後訓練會被停止。

---

###Early Stop Patience
```python
keras.callbacks.EarlyStopping(monitor='val_loss',patience=0)
```
當被監測的validation loss不再下降，則停止訓練。
####參數
* **patience**：沒有進步的訓練輪數，在這之後訓練會被停止。













